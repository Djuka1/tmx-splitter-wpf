﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMXSplitter.Lib
{
    public class TmxPair
    {
        public int Num { get; set; }
        public string English { get; set; }
        public string Serbian { get; set; }
    }
}
