﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TMXSplitter.Lib
{
    public class TmxUtilities
    {
        public static IQueryable<TmxPair> GetTmxPairs(string path)
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.Load(path);

                XmlNodeList bodyNodes = document.SelectNodes("//body/tu/tuv/seg");

                List<TmxPair> tmxPairs = new List<TmxPair>();
                int tmxCounter = 1;
                for (int i = 0; i < bodyNodes.Count; i += 2)
                {
                    string english = bodyNodes[i].FirstChild.InnerText;
                    string serbian = bodyNodes[i + 1].FirstChild.InnerText;
                    TmxPair pair = new TmxPair { Num= tmxCounter++, English = english, Serbian = serbian };
                    tmxPairs.Add(pair);
                }

                return tmxPairs.AsQueryable();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
