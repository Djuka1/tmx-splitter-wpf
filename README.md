TMX File Splitter WPF
===

Project done in WPF to read contents of a .tmx file in a relatively clean way.
Tmx files are esentially just .xml files for translators, as I have gathered from friends. The need to write this little program was to help out a friend who couldn't find a free and easy way to interpret .tmx files.

The file reader looks for a tag sequences like and will try to make pairs of tag sequences. The structure of the file should be like:

&lt;body&gt; <br>
... <br>
&lt;tu&gt;<br>
&lt;tuv&gt;&lt;seg&gt;This is a sentence in some language.&lt;/seg&gt;&lt;/tuv&gt;<br>
&lt;tuv&gt;&lt;seg&gt;This is the translation in another language.&lt;/seg&gt;&lt;/tuv&gt;<br>
&lt;/tu&gt;<br>
...
&lt;tu&gt;<br>
&lt;tuv&gt;&lt;seg&gt;This is another sentence in a language&lt;/seg&gt;&lt;/tuv&gt;<br>
&lt;tuv&gt;&lt;seg&gt;And this is another translation of that language&lt;/seg&gt;&lt;/tuv&gt;<br>
&lt;/tu&gt;<br>
...<br>
&lt;/body&gt;

The program has a file chooser which can take any files, but it will probably fail if no .xml or .tmx files are chosen. It will also try to make pairs of 'tuv -> seg' tag sequences. So if there are some sequences that are not paired up the program will probably fail.

The program also has a basic search function that looks at every pair loaded, and will filter all rows that do not contain the search term.

Paging of the file is done in 100's, so the table will show 100 rows for the total number of pairs on each page, and you can navigate going forward a 100 rows and backwards a 100 rows. There is also an input field to enter the page, for instance, entering page 2 will give you rows from 101 to 200.

There is no install, download the TMXPackage directory and run TMXSplitter.WPF.exe and that's it. Have fun and spread it out.