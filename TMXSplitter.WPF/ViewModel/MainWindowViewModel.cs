﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TMXSplitter.Lib;
using TMXSplitter.WPF.Commands;

namespace TMXSplitter.WPF.ViewModel
{
    public class MainWindowViewModel : BaseModel
    {
        #region FIELDS
        private int pageSize = 100;

        private IQueryable<TmxPair> _allPairs = new List<TmxPair>().AsQueryable();
        private IQueryable<TmxPair> _totalNonVisible = new List<TmxPair>().AsQueryable();

        #endregion

        #region PROPERTIES
        private IQueryable<TmxPair> _tmxPairs;
        public IQueryable<TmxPair> TmxPairs
        {
            get
            {
                return _tmxPairs;
            }

            set
            {
                _tmxPairs = value;
                OnPropertyChanged("TmxPairs");
            }
        }

        private int _currentPage;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                OnPropertyChanged("CurrentPage");
                UpdateVisiblePages();
            }
        }

        private string _searchText = "";
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                OnPropertyChanged("SearchText");
                CurrentPage = 1;
            }
        }

        private string _numberOfPages;

        public string NumberOfPages
        {
            get { return _numberOfPages; }
            set { _numberOfPages = value; OnPropertyChanged("NumberOfPages"); }
        }


        #endregion

        #region COMMANDS
        private ICommand _previous100;
        public ICommand Previous100
        {
            get
            {
                if (_previous100 == null)
                {
                    _previous100 = new RelayCommand(
                        (x) => CurrentPage > 1,
                        (x) => Previous100Fn()
                    );
                }
                return _previous100;
            }
        }
        public void Previous100Fn()
        {
            CurrentPage -= 1;
        }

        private ICommand _next100;
        public ICommand Next100
        {
            get
            {
                if (_next100 == null)
                {
                    _next100 = new RelayCommand(
                        (x) => (_totalNonVisible.Count() + pageSize - 1) / pageSize != CurrentPage,
                        (x) => Next100Fn()
                    );
                }
                return _next100;
            }
        }
        public void Next100Fn()
        {
            CurrentPage += 1;
        }

        private ICommand _loadDocument;
        public ICommand LoadDocument
        {
            get
            {
                if (_loadDocument == null)
                {
                    _loadDocument = new RelayCommand((x) => true, (x) => LoadDocumentFunction());
                }
                return _loadDocument;
            }
        }
        private void LoadDocumentFunction()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = "*";

            Nullable<bool> result = dlg.ShowDialog();

            if (result != true)
            {
                return;
            }

            string filename = dlg.FileName;
            _allPairs = TmxUtilities.GetTmxPairs(filename);
            if (_allPairs == null)
            {
                MessageBox.Show("Error loading file.");
                return;
            }

            CurrentPage = 1;
        }

        #endregion

        #region METHODS
        private void UpdateVisiblePages()
        {
            //TmxPairs 
            _totalNonVisible = _allPairs.Where(
                   x => x.English.ToLower().Contains(SearchText.ToLower())
                   || x.Serbian.ToLower().Contains(SearchText.ToLower())
                   );
            TmxPairs = _totalNonVisible.Skip((CurrentPage - 1) * pageSize).Take(pageSize);
            NumberOfPages = _totalNonVisible.Count() + "";
        }
        #endregion
    }
}
